# PlainAcct

Plain double-entry accounting application written in [Django](https://www.djangoproject.com).

The project started as a Django demonstration project with a group of students at SŠIPS.

## Docker Container

The [Docker](https://www.docker.com) container built using the provided Dockerfile is not runnable out-of-the-box. It is intended as a base container for Docker Compose and the app should be started using the `command` config option. For example, using

	command: python manage.py runserver

in a development environment and

	command: gunicorn plainacct.wsgi:application

in a production environment. See the [Docker Compose manual](https://docs.docker.com/compose/) for details.
