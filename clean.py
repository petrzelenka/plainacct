#!/usr/bin/env python
"""Command-line utility for cleaning run-time files."""
from pathlib import Path


def clean():
	working_dir = Path.cwd()

	# Remove .pyc and .pyo files
	for path in working_dir.rglob("*py[co]"):
		path.unlink()

	# Remove __pycache__ directories
	for path in working_dir.rglob("__pycache__"):
		path.rmdir()


if __name__ == "__main__":
	clean()
