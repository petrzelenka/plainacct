FROM python:3.9.5-alpine3.13

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update
RUN apk add --virtual psycopg2-build-deps gcc musl-dev postgresql-dev python3-dev

RUN mkdir -p /opt/plainacct/static
WORKDIR /opt/plainacct

COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . .
RUN addgroup --system plainacct
RUN adduser --system plainacct --ingroup plainacct
RUN chown -R plainacct:plainacct /opt/plainacct
RUN chmod -R o-rwx /opt/plainacct

RUN apk del psycopg2-build-deps
RUN apk add postgresql-libs
