# Generated by Django 3.2 on 2021-04-21 18:45

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

	initial = True

	dependencies = [
		('auth', '0012_alter_user_first_name_max_length'),
	]

	operations = [
		migrations.CreateModel(
			name='Accountant',
			fields=[
				('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
				('password', models.CharField(max_length=128, verbose_name='password')),
				('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
				('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
				('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
				('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
				('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
				('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
				('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
				('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
				('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
				('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
				('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
			],
			options={
				'verbose_name': 'user',
				'verbose_name_plural': 'users',
				'abstract': False,
			},
			managers=[
				('objects', django.contrib.auth.models.UserManager()),
			],
		),
		migrations.CreateModel(
			name='Account',
			fields=[
				('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
				('number', models.CharField(max_length=3, validators=[django.core.validators.RegexValidator('^[\\d]{3}$', message='Account number must consist of exactly three digits.')], verbose_name='Primary account number')),
				('subnumber', models.CharField(blank=True, default=None, max_length=3, null=True, validators=[django.core.validators.RegexValidator('^[\\d]{3}$', message='Account subnumber must consist of exactly three digits.')], verbose_name='Subaccount number')),
				('title', models.CharField(max_length=100, verbose_name='Title')),
				('category', models.CharField(choices=[('BA', 'Balance (asset)'), ('BE', 'Balance (equity)'), ('BI', 'Balance (asset or liability)'), ('BL', 'Balance (liability)'), ('EP', 'Expense'), ('RV', 'Revenue'), ('SC', 'Special (closing)'), ('SO', 'Special (opening)'), ('SP', 'Special (profit and loss)')], max_length=2, verbose_name='Category')),
				('enabled', models.BooleanField(default=True, verbose_name='Enabled')),
			],
		),
		migrations.CreateModel(
			name='AccountingPeriod',
			fields=[
				('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
				('title', models.CharField(max_length=50, unique=True, verbose_name='Title')),
				('opening_day', models.DateField(verbose_name='Opening date')),
				('closing_day', models.DateField(verbose_name='Closing date')),
				('state', models.CharField(choices=[('F', 'Future'), ('C', 'Current'), ('P', 'Past')], default='F', max_length=1, verbose_name='State')),
			],
		),
		migrations.CreateModel(
			name='DocumentType',
			fields=[
				('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
				('abbreviation', models.CharField(max_length=3, unique=True, verbose_name='Abbreviation')),
				('title', models.CharField(max_length=50, unique=True, verbose_name='Title')),
			],
		),
		migrations.CreateModel(
			name='Transaction',
			fields=[
				('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
				('sequence_number', models.PositiveIntegerField(default=None, null=True, unique=True, verbose_name='Sequence number')),
				('booking_date', models.DateField(verbose_name='Booking date')),
				('document_number', models.PositiveIntegerField(verbose_name='Document number')),
				('description', models.TextField(max_length=255, verbose_name='Description')),
				('amount', models.DecimalField(decimal_places=2, max_digits=17, verbose_name='Amount')),
				('business_partner', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='Business partner')),
				('tax_deductible_expense', models.BooleanField(default=True, verbose_name='Tax-deductible expense')),
				('remark', models.TextField(blank=True, default=None, max_length=5000, null=True, verbose_name='Remark')),
				('accounting_period', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.accountingperiod', verbose_name='Accounting period')),
				('booked_by', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Booked by')),
				('credit_account', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='+', to='core.account', verbose_name='Credit account')),
				('debit_account', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='+', to='core.account', verbose_name='Debit account')),
				('document_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='+', to='core.documenttype', verbose_name='Document type')),
			],
		),
	]
