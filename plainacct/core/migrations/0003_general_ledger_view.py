from django.apps.registry import Apps
from django.db import migrations
from django.db.backends.base.schema import BaseDatabaseSchemaEditor

OBJECT_NAME = "core_general_ledger"

SQL_SELECT = """
	SELECT
		2 * id - 1 AS id,
		sequence_number,
		accounting_period_id,
		booking_date,
		document_type_id,
		document_number,
		description,
		debit_account_id AS account_id,
		amount AS amount_debit,
		0 AS amount_credit,
		business_partner,
		tax_deductible_expense,
		remark,
		booked_by_id
	FROM core_transaction
	UNION
	SELECT
		2 * id AS id,
		sequence_number,
		accounting_period_id,
		booking_date,
		document_type_id,
		document_number,
		description,
		credit_account_id AS account_id,
		0 AS amount_debit,
		amount AS amount_credit,
		business_partner,
		tax_deductible_expense,
		remark,
		booked_by_id
	FROM core_transaction
"""

SQL_STATEMENTS = {
	"mysql": [
		f"CREATE OR REPLACE VIEW {OBJECT_NAME} AS {SQL_SELECT};"
	],

	"postgresql": [
		f"CREATE OR REPLACE VIEW {OBJECT_NAME} AS {SQL_SELECT};"
	],

	"sqlite": [
		f"DROP VIEW IF EXISTS {OBJECT_NAME};",
		f"CREATE VIEW {OBJECT_NAME} AS {SQL_SELECT};"
	],
}


def execute_sql(apps: Apps, schema_editor: BaseDatabaseSchemaEditor) -> None: # pylint: disable=unused-argument
	database_vendor = schema_editor.connection.vendor
	for statement in SQL_STATEMENTS[database_vendor]:
		schema_editor.execute(statement)


class Migration(migrations.Migration):

	dependencies = [
		('core', '0002_initial_accounts'),
	]

	operations = [
		migrations.RunPython(execute_sql, atomic=False)
	]
