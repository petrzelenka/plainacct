from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.admin import SimpleListFilter
from django.db.models import Q
from django.db.models.query import QuerySet
from django.http import HttpRequest
from django.utils.translation import pgettext

from .models import Account
from .models import Accountant
from .models import AccountingPeriod
from .models import DocumentType

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class ChartOfAccountsSectionFilter(SimpleListFilter):

	parameter_name = "section"

	title = pgettext("account admin", "Chart of accounts section")


	def lookups(self, request: HttpRequest, model_admin: "AccountAdmin") -> tuple[tuple[str, str], ...]:
		return (
			("0", pgettext("account admin", "Section 0 (0xx.xxx)")),
			("1", pgettext("account admin", "Section 1 (1xx.xxx)")),
			("2", pgettext("account admin", "Section 2 (2xx.xxx)")),
			("3", pgettext("account admin", "Section 3 (3xx.xxx)")),
			("4", pgettext("account admin", "Section 4 (4xx.xxx)")),
			("5", pgettext("account admin", "Section 5 (5xx.xxx)")),
			("6", pgettext("account admin", "Section 6 (6xx.xxx)")),
			("7", pgettext("account admin", "Section 7 (7xx.xxx)")),
			("8", pgettext("account admin", "Section 8 (8xx.xxx)")),
			("9", pgettext("account admin", "Section 9 (9xx.xxx)")),
		)

	def queryset(self, request: HttpRequest, queryset: QuerySet) -> QuerySet:
		if self.value():
			return queryset.filter(Q(number__startswith=self.value()))
		else:
			return queryset


@admin.action(description=pgettext("account admin", "Disable selected accounts"))
def disable_accounts(modeladmin: "AccountAdmin", request: HttpRequest, queryset: QuerySet) -> None: # pylint: disable=unused-argument
	queryset.update(enabled=False)

@admin.action(description=pgettext("account admin", "Enable selected accounts"))
def enable_accounts(modeladmin: "AccountAdmin", request: HttpRequest, queryset: QuerySet) -> None: # pylint: disable=unused-argument
	queryset.update(enabled=True)


@admin.register(Account)
class AccountAdmin(ModelAdmin):

	actions = (
		disable_accounts,
		enable_accounts,
	)

	list_display = (
		"compound_number",
		"title",
		"category",
		"enabled",
	)

	list_filter = (
		"enabled",
		ChartOfAccountsSectionFilter,
		"category",
	)

	ordering = (
		"number",
		"subnumber",
	)

	readonly_fields = (
		"compound_number",
	)

	search_fields = (
		"number",
		"subnumber",
		"title"
	)

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

@admin.register(AccountingPeriod)
class AccountingPeriodAdmin(ModelAdmin):

	list_display = (
		"title",
		"opening_day",
		"closing_day",
		"state",
	)

	list_filter = (
		"state",
	)

	ordering = (
		"opening_day",
	)

	search_fields = (
		"title",
	)

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

@admin.register(Accountant)
class AccountantAdmin(ModelAdmin):

	list_display = (
		"username",
		"first_name",
		"last_name",
		"is_staff",
		"is_superuser",
		"is_active",
	)

	list_filter = (
		"is_staff",
		"is_superuser",
		"is_active",
	)

	ordering = (
		"last_name",
		"first_name",
	)

	search_fields = (
		"username",
		"first_name",
		"last_name",
	)

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

@admin.register(DocumentType)
class DocumentTypeAdmin(ModelAdmin):

	list_display = (
		"abbreviation",
		"title",
	)

	ordering = (
		"abbreviation",
	)

	search_fields = (
		"abbreviation",
		"title",
	)
