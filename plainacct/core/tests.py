from django.test import TestCase

from .models import Account

# pylint: disable=no-member

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class AccountTest(TestCase):

	def test_is_subaccount(self):
		self.assertFalse(
			Account(number="603", title="Tuition").is_subaccount()
		)
		self.assertTrue(
			Account(number="603", subnumber="001", title="Tuition (Gryffindor)").is_subaccount()
		)
		self.assertTrue(
			Account(number="603", subnumber="002", title="Tuition (Hufflepuff)").is_subaccount()
		)
		self.assertTrue(
			Account(number="603", subnumber="003", title="Tuition (Ravenclaw)").is_subaccount()
		)
		self.assertTrue(
			Account(number="603", subnumber="004", title="Tuition (Slytherin)").is_subaccount()
		)

	def test_str(self):
		self.assertEqual(
			str(Account(number="603", title="Tuition")),
			"603 - Tuition"
		)
		self.assertEqual(
			str(Account(number="603", subnumber="001", title="Tuition (Gryffindor)")),
			"603.001 - Tuition (Gryffindor)"
		)
		self.assertEqual(
			str(Account(number="603", subnumber="002", title="Tuition (Hufflepuff)")),
			"603.002 - Tuition (Hufflepuff)"
		)
		self.assertEqual(
			str(Account(number="603", subnumber="003", title="Tuition (Ravenclaw)")),
			"603.003 - Tuition (Ravenclaw)"
		)
		self.assertEqual(
			str(Account(number="603", subnumber="004", title="Tuition (Slytherin)")),
			"603.004 - Tuition (Slytherin)"
		)
