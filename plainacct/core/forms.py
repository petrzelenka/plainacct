from datetime import date
from typing import Any

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import PasswordChangeForm
from django.core.validators import ValidationError
from django.db.models import Exists
from django.db.models import Max
from django.db.models import OuterRef
from django.db.models import Q
from django.forms import CharField
from django.forms import DateField
from django.forms import DateInput
from django.forms import Form
from django.forms import ModelChoiceField
from django.forms import ModelForm
from django.forms import NumberInput
from django.forms import PasswordInput
from django.forms import Textarea
from django.forms import TextInput
from django.utils.translation import pgettext

from ..utils.forms import StyledErrorForm
from .models import Account
from .models import AccountCategory
from .models import AccountingPeriod
from .models import DocumentType
from .models import Transaction

# pylint: disable=no-member

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class ChangeAccountingPeriodForm(Form):

	accounting_period = ModelChoiceField(
		label=pgettext("transaction field", "Accounting period"),
		empty_label=None,
		required=True,
		queryset=AccountingPeriod.objects.all()
	)


class DocumentsFilterForm(Form):

	document_type = ModelChoiceField(
		label=pgettext("transaction field", "Document type"),
		empty_label=pgettext("filter", "(all)"),
		required=False,
		queryset=DocumentType.objects.all().order_by("abbreviation")
	)

	start_date = DateField(
		label=pgettext("filter", "Start date"),
		required=False,
		widget=DateInput(format="%Y-%m-%d", attrs={ "class": "input", "type": "date" }),
	)

	end_date = DateField(
		label=pgettext("filter", "End date"),
		required=False,
		widget=DateInput(format="%Y-%m-%d", attrs={ "class": "input", "type": "date" }),
	)


	def clean(self) -> dict[str, Any]:
		"""Validate this form"""
		super().clean()

		start_date = self.cleaned_data["start_date"]
		end_date = self.cleaned_data["end_date"]

		if start_date and end_date:
			self.cleaned_data["start_date"] = min(start_date, end_date)
			self.cleaned_data["end_date"] = max(start_date, end_date)

		return self.cleaned_data


class GeneralLedgerFilterForm(Form):

	account = ModelChoiceField(
		label=pgettext("transaction report", "Account"),
		empty_label=None,
		required=False,
		queryset=Account.objects\
				.filter(Q(enabled=True))\
				.order_by("number", "subnumber")
	)

	start_date = DateField(
		label=pgettext("filter", "Start date"),
		required=False,
		widget=DateInput(format="%Y-%m-%d", attrs={ "class": "input", "type": "date" }),
	)

	end_date = DateField(
		label=pgettext("filter", "End date"),
		required=False,
		widget=DateInput(format="%Y-%m-%d", attrs={ "class": "input", "type": "date" }),
	)

	fulltext = CharField(
		label=pgettext("filter", "Fulltext search"),
		required=False,
		widget=TextInput(attrs={ "class": "input", "placeholder": pgettext("filter", "Search in description, business partner, and remark") }),
	)


	def clean(self) -> dict[str, Any]:
		"""Validate this form"""
		super().clean()

		start_date = self.cleaned_data["start_date"]
		end_date = self.cleaned_data["end_date"]

		if start_date and end_date:
			self.cleaned_data["start_date"] = min(start_date, end_date)
			self.cleaned_data["end_date"] = max(start_date, end_date)

		return self.cleaned_data


class JournalFilterForm(Form):

	start_date = DateField(
		label=pgettext("filter", "Start date"),
		required=False,
		widget=DateInput(format="%Y-%m-%d", attrs={ "class": "input", "type": "date" }),
	)

	end_date = DateField(
		label=pgettext("filter", "End date"),
		required=False,
		widget=DateInput(format="%Y-%m-%d", attrs={ "class": "input", "type": "date" }),
	)

	fulltext = CharField(
		label=pgettext("filter", "Fulltext search"),
		required=False,
		widget=TextInput(attrs={ "class": "input", "placeholder": pgettext("filter", "Search in description, business partner, and remark") }),
	)


	def clean(self) -> dict[str, Any]:
		"""Validate this form"""
		super().clean()

		start_date = self.cleaned_data["start_date"]
		end_date = self.cleaned_data["end_date"]

		if start_date and end_date:
			self.cleaned_data["start_date"] = min(start_date, end_date)
			self.cleaned_data["end_date"] = max(start_date, end_date)

		return self.cleaned_data


class LoginForm(AuthenticationForm, StyledErrorForm):

	error_css_class = "is-danger"

	username = CharField(
		label=pgettext("profile", "User name"),
		required=True,
		widget=TextInput(attrs={ "class": "input" })
	)

	password = CharField(
		label=pgettext("profile", "Password"),
		required=True,
		widget=PasswordInput(attrs={ "class": "input" })
	)

class PasswordChangeForm(PasswordChangeForm, StyledErrorForm):

	error_css_class = "is-danger"

	old_password = CharField(
		label=pgettext("profile", "Old password"),
		required=True,
		widget=PasswordInput(attrs={ "class": "input" })
	)

	new_password1 = CharField(
		label=pgettext("profile", "New password"),
		required=True,
		widget=PasswordInput(attrs={ "class": "input" })
	)

	new_password2 = CharField(
		label=pgettext("profile", "New password (again)"),
		required=True,
		widget=PasswordInput(attrs={ "class": "input" })
	)


class TransactionEntryForm(ModelForm, StyledErrorForm):

	error_css_class = "is-danger"

	class Meta:

		exclude = ( "sequence_number", "booked_by" )

		model = Transaction

		widgets = {
			"amount": NumberInput(
				attrs={ "class": "input" }),
			"booking_date": DateInput(
				format="%Y-%m-%d", attrs={ "class": "input", "type": "date" }),
			"business_partner": TextInput(
				attrs={ "class": "input" }),
			"description": TextInput(
				attrs={ "class": "input" }),
			"document_number": NumberInput(
				attrs={ "class": "input" }),
			"remark": Textarea(
				attrs={ "class": "textarea", "rows": 5 }),
		}


	def __init__(self, *args, **kwargs):
		"""Initialize this form"""
		super().__init__(*args, **kwargs)

		accounts = Account.objects.filter(
			Q(enabled=True)
		)
		accounts = accounts.exclude( # Exclude accounts with existing subaccounts
			Q(subnumber__isnull=True) &
			Q(Exists(
				Account.objects.filter(
					Q(number=OuterRef("number")) &
					Q(subnumber__isnull=False))
				)
			)
		)
		accounts = accounts.order_by("number", "subnumber")

		self.fields["credit_account"].empty_label = ""
		self.fields["credit_account"].queryset = accounts
		self.fields["debit_account"].empty_label = ""
		self.fields["debit_account"].queryset = accounts

		document_types = DocumentType.objects\
				.all()\
				.order_by("abbreviation")

		self.fields["document_type"].empty_label = ""
		self.fields["document_type"].queryset = document_types

	def clean(self) -> dict[str, Any]:
		"""Validate this form"""
		super().clean()

		# Document numbers must form a sequence for the respective document type
		document_type = self.cleaned_data.get("document_type")
		document_number = self.cleaned_data.get("document_number")

		last_number_used = Transaction.objects\
			.filter(Q(document_type=document_type))\
			.aggregate(Max("document_number"))\
			["document_number__max"]

		if last_number_used and document_number != last_number_used + 1:
			self.add_error("document_number", ValidationError(pgettext("transaction validation", "Document number is out of sequence.")))

		# Expense accounts must not be used on credit side
		debit_account = self.cleaned_data.get("debit_account")
		if debit_account and debit_account.category == AccountCategory.REVENUE:
			self.add_error("debit_account", ValidationError(pgettext("transaction validation", "Revenue accounts must not be used on debit side.")))

		# Revenue accounts must not be used on debit side
		credit_account = self.cleaned_data.get("credit_account")
		if credit_account and credit_account.category == AccountCategory.EXPENSE:
			self.add_error("credit_account", ValidationError(pgettext("transaction validation", "Expense accounts must not be used on credit side.")))

		# Unset tax-deductible expense flag if no expense account used
		if debit_account and debit_account.category != AccountCategory.EXPENSE:
			self.cleaned_data["tax_deductible_expense"] = False

		return self.cleaned_data
