from decimal import Decimal
from typing import Any
from typing import Dict
from typing import Iterable
from typing import Tuple

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Count
from django.db.models import Max
from django.db.models import Min
from django.db.models import Q
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from ..utils import replace_none_by_default
from ..utils.views import LastPageByDefaultListView
from .forms import ChangeAccountingPeriodForm
from .forms import DocumentsFilterForm
from .forms import GeneralLedgerFilterForm
from .forms import JournalFilterForm
from .forms import TransactionEntryForm
from .models import Account
from .models import DocumentType
from .models import AccountingPeriod
from .models import AccountingPeriodState
from .models import GeneralLedgerEntry
from .models import Transaction

# pylint: disable=no-member

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

def get_current_accounting_period(request: HttpRequest) -> AccountingPeriod:
	try:
		return AccountingPeriod(**request.session["current_accounting_period"])
	except KeyError:
		try:
			current_accounting_period = AccountingPeriod.objects.get(
				Q(state=AccountingPeriodState.CURRENT)
			)
			request.session["current_accounting_period"] = model_to_dict(current_accounting_period)

			return current_accounting_period
		except AccountingPeriod.DoesNotExist:
			return None

class InjectAccountingPeriodMixin:

	def get_context_data(self, **kwargs) -> Dict[str, Any]:
		context = super().get_context_data(**kwargs)
		context["current_accounting_period"] = get_current_accounting_period(self.request)

		return context

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class DocumentDetailView(InjectAccountingPeriodMixin, LoginRequiredMixin, PermissionRequiredMixin, LastPageByDefaultListView):

	context_object_name = "document_entries"

	model = Transaction

	paginate_by = 10

	permission_required = "core.view_transaction"

	template_name = "core/document_detail.html"


	def get_context_data(self, **kwargs) -> Dict[str, Any]:
		context = super().get_context_data(**kwargs)
		current_accounting_period = get_current_accounting_period(self.request)

		filter_form = JournalFilterForm(
			initial=replace_none_by_default(self.filter_form.cleaned_data, {
				"start_date": current_accounting_period.opening_day,
				"end_date": current_accounting_period.closing_day,
				"fulltext": "",
			})
		)
		context["filter_form"] = filter_form

		context["document_type"] = DocumentType.objects.get(Q(pk=self.kwargs["document_type_id"]))
		context["document_number"] = self.kwargs["document_number"]

		return context

	def get_queryset(self) -> QuerySet:
		current_accounting_period = get_current_accounting_period(self.request)

		filter_queries = [
			Q(accounting_period__id=current_accounting_period.id),
			Q(document_type=self.kwargs["document_type_id"]),
			Q(document_number=self.kwargs["document_number"]),
		]

		self.filter_form = JournalFilterForm(self.request.GET)
		if self.filter_form.is_valid():
			# Filter using start date
			start_date = self.filter_form.cleaned_data["start_date"]
			if start_date:
				filter_queries.append(Q(booking_date__gte=start_date))

			# Filter using end date
			end_date = self.filter_form.cleaned_data["end_date"]
			if end_date:
				filter_queries.append(Q(booking_date__lte=end_date))

			# Filter using fulltext search
			fulltext_term = self.filter_form.cleaned_data["fulltext"]
			if fulltext_term:
				filter_queries.append(
					Q(description__icontains=fulltext_term) | Q(business_partner__icontains=fulltext_term) | Q(remark__icontains=fulltext_term)
				)

		return Transaction.objects.filter(*filter_queries)


# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class DocumentsView(InjectAccountingPeriodMixin, LoginRequiredMixin, PermissionRequiredMixin, LastPageByDefaultListView):

	context_object_name = "documents"

	model = Transaction

	paginate_by = 10

	permission_required = "core.view_transaction"

	template_name = "core/documents.html"


	def get_context_data(self, **kwargs) -> Dict[str, Any]:
		context = super().get_context_data(**kwargs)
		current_accounting_period = get_current_accounting_period(self.request)

		filter_form = DocumentsFilterForm(
			initial=replace_none_by_default(self.filter_form.cleaned_data, {
				"start_date": current_accounting_period.opening_day,
				"end_date": current_accounting_period.closing_day,
			})
		)
		context["filter_form"] = filter_form

		return context

	def get_queryset(self) -> QuerySet:
		current_accounting_period = get_current_accounting_period(self.request)

		filter_queries = [
			Q(accounting_period__id=current_accounting_period.id),
		]

		self.filter_form = DocumentsFilterForm(self.request.GET)
		if self.filter_form.is_valid():
			# Filter using document type
			document_type = self.filter_form.cleaned_data["document_type"]
			if document_type:
				filter_queries.append(Q(document_type=document_type))

			# Filter using start date
			start_date = self.filter_form.cleaned_data["start_date"]
			if start_date:
				filter_queries.append(Q(booking_date__gte=start_date))

			# Filter using end date
			end_date = self.filter_form.cleaned_data["end_date"]
			if end_date:
				filter_queries.append(Q(booking_date__lte=end_date))

		return Transaction.objects\
				.filter(*filter_queries)\
				.values("document_type", "document_type__abbreviation", "document_number")\
				.annotate(min_date=Min("booking_date"), max_date=Max("booking_date"), entries=Count("amount"))\
				.order_by("max_date")


class GeneralLedgerView(InjectAccountingPeriodMixin, LoginRequiredMixin, PermissionRequiredMixin, ListView):

	context_object_name = "general_ledger_entries"

	model = GeneralLedgerEntry

	permission_required = "core.view_transaction"

	template_name = "core/general_ledger.html"


	@staticmethod
	def calculate_balance(entries: Iterable[GeneralLedgerEntry]) -> Tuple[Decimal, Decimal]:
		sum_debit = sum(( entry.amount_debit for entry in entries ))
		sum_credit = sum(( entry.amount_credit for entry in entries ))

		if sum_debit > sum_credit:
			balance_debit = Decimal(sum_debit - sum_credit)
			balance_credit = Decimal(0)

		if sum_debit < sum_credit:
			balance_debit = Decimal(0)
			balance_credit = Decimal(sum_credit - sum_debit)

		if sum_debit == sum_credit:
			balance_debit = Decimal(0)
			balance_credit = Decimal(0)

		return balance_debit, balance_credit

	def get_context_data(self, **kwargs) -> Dict[str, Any]:
		context = super().get_context_data(**kwargs)
		current_accounting_period = get_current_accounting_period(self.request)

		filter_form = GeneralLedgerFilterForm(
			initial=replace_none_by_default(self.filter_form.cleaned_data, {
				"account": self.get_default_account(),
				"start_date": current_accounting_period.opening_day,
				"end_date": current_accounting_period.closing_day,
				"fulltext": "",
			})
		)
		context["filter_form"] = filter_form

		entries = context[self.context_object_name]
		balance_debit, balance_credit = self.calculate_balance(entries)
		context["balance_debit"] = balance_debit
		context["balance_credit"] = balance_credit

		return context

	@staticmethod
	def get_default_account() -> Account:
		return Account.objects.get(Q(number=221), Q(subnumber__isnull=True))

	def get_queryset(self) -> QuerySet:
		current_accounting_period = get_current_accounting_period(self.request)

		filter_queries = [
			Q(accounting_period__id=current_accounting_period.id),
		]

		self.filter_form = GeneralLedgerFilterForm(self.request.GET)
		if self.filter_form.is_valid():
			# Filter using account
			account = self.filter_form.cleaned_data["account"]
			if not account:
				account = self.get_default_account()

			filter_queries.append(Q(account__number=account.number))
			if account.subnumber:
				filter_queries.append(Q(account__subnumber=account.subnumber))

			# Filter using start date
			start_date = self.filter_form.cleaned_data["start_date"]
			if start_date:
				filter_queries.append(Q(booking_date__gte=start_date))

			# Filter using end date
			end_date = self.filter_form.cleaned_data["end_date"]
			if end_date:
				filter_queries.append(Q(booking_date__lte=end_date))

			# Filter using fulltext search
			fulltext_term = self.filter_form.cleaned_data["fulltext"]
			if fulltext_term:
				filter_queries.append(
					Q(description__icontains=fulltext_term) | Q(business_partner__icontains=fulltext_term) | Q(remark__icontains=fulltext_term)
				)

		return GeneralLedgerEntry.objects.filter(*filter_queries)


class JournalView(InjectAccountingPeriodMixin, LoginRequiredMixin, PermissionRequiredMixin, LastPageByDefaultListView):

	context_object_name = "journal_entries"

	model = Transaction

	paginate_by = 10

	permission_required = "core.view_transaction"

	template_name = "core/journal.html"


	def get_context_data(self, **kwargs) -> Dict[str, Any]:
		context = super().get_context_data(**kwargs)
		current_accounting_period = get_current_accounting_period(self.request)

		filter_form = JournalFilterForm(
			initial=replace_none_by_default(self.filter_form.cleaned_data, {
				"start_date": current_accounting_period.opening_day,
				"end_date": current_accounting_period.closing_day,
				"fulltext": "",
			})
		)
		context["filter_form"] = filter_form

		return context

	def get_queryset(self) -> QuerySet:
		current_accounting_period = get_current_accounting_period(self.request)

		filter_queries = [
			Q(accounting_period__id=current_accounting_period.id),
		]

		self.filter_form = JournalFilterForm(self.request.GET)
		if self.filter_form.is_valid():
			# Filter using start date
			start_date = self.filter_form.cleaned_data["start_date"]
			if start_date:
				filter_queries.append(Q(booking_date__gte=start_date))

			# Filter using end date
			end_date = self.filter_form.cleaned_data["end_date"]
			if end_date:
				filter_queries.append(Q(booking_date__lte=end_date))

			# Filter using fulltext search
			fulltext_term = self.filter_form.cleaned_data["fulltext"]
			if fulltext_term:
				filter_queries.append(
					Q(description__icontains=fulltext_term) | Q(business_partner__icontains=fulltext_term) | Q(remark__icontains=fulltext_term)
				)

		return Transaction.objects.filter(*filter_queries)


class TransactionCreateView(InjectAccountingPeriodMixin, LoginRequiredMixin, PermissionRequiredMixin, CreateView):

	form_class = TransactionEntryForm

	success_url = reverse_lazy("journal")

	permission_required = "core.add_transaction"

	template_name = "core/transaction_create.html"


	def form_valid(self, form: TransactionEntryForm):
		form.instance.booked_by = self.request.user

		return super().form_valid(form)

	def get_initial(self):
		initial = super().get_initial()
		initial["accounting_period"] = get_current_accounting_period(self.request)

		return initial


class TransactionDetailView(InjectAccountingPeriodMixin, LoginRequiredMixin, PermissionRequiredMixin, DetailView):

	context_object_name = "transaction"

	model = Transaction

	permission_required = "core.view_transaction"

	template_name = "core/transaction_detail.html"

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

@login_required
def change_accounting_period(request: HttpRequest) -> HttpResponse:
	if request.method == "POST":
		form = ChangeAccountingPeriodForm(request.POST)
		if form.is_valid():
			current_accounting_period = form.cleaned_data["accounting_period"]
			request.session["current_accounting_period"] = model_to_dict(current_accounting_period)

			return HttpResponseRedirect(reverse_lazy("core_dashboard"))
	else:
		current_accounting_period = get_current_accounting_period(request)
		form = ChangeAccountingPeriodForm(initial={
			"accounting_period": get_current_accounting_period(request),
		})

	return render(request, "core/change_accounting_period.html", {
		"current_accounting_period": current_accounting_period,
		"form": form,
	})

@login_required
def dashboard(request: HttpRequest) -> HttpResponse:
	return render(request, "core/dashboard.html", {
		"current_accounting_period": get_current_accounting_period(request)
	})
