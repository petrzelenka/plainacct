from django.urls import path

from . import views

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

urlpatterns = (
	path("", views.dashboard, name="core_dashboard"),
	path("documents/", views.DocumentsView.as_view(), name="documents"),
	path("documents/<int:document_type_id>/<int:document_number>", views.DocumentDetailView.as_view(), name="document_detail"),
	path("general-ledger/", views.GeneralLedgerView.as_view(), name="general_ledger"),
	path("journal/", views.JournalView.as_view(), name="journal"),
	path("periods/change/", views.change_accounting_period, name="change_accounting_period"),
	path("transactions/<int:pk>/", views.TransactionDetailView.as_view(), name="transaction_detail"),
	path("transactions/create", views.TransactionCreateView.as_view(), name="transaction_create"),
)
