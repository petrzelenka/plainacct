from __future__ import annotations

from django.contrib import admin
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import NON_FIELD_ERRORS
from django.core.validators import RegexValidator
from django.core.validators import ValidationError
from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateField
from django.db.models import DecimalField
from django.db.models import DO_NOTHING
from django.db.models import ForeignKey
from django.db.models import PositiveIntegerField
from django.db.models import PROTECT
from django.db.models import Model
from django.db.models import Q
from django.db.models import TextChoices
from django.db.models import TextField
from django.db.models import Value
from django.db.models.functions import Concat
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils import dateformat
from django.utils import formats
from django.utils.timezone import localdate
from django.utils.translation import get_language
from django.utils.translation import pgettext

# pylint: disable=no-member

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class AccountCategory(TextChoices):

	BALANCE_ASSET = "BA", pgettext("account category", "Balance (asset)")

	BALANCE_EQUITY = "BE", pgettext("account category", "Balance (equity)")

	BALANCE_INDETERMINATE = "BI", pgettext("account category", "Balance (asset or liability)")

	BALANCE_LIABILITY = "BL", pgettext("account category", "Balance (liability)")

	EXPENSE = "EP", pgettext("account category", "Expense")

	REVENUE = "RV", pgettext("account category", "Revenue")

	SPECIAL_CLOSING = "SC", pgettext("account category", "Special (closing)")

	SPECIAL_OPENING = "SO", pgettext("account category", "Special (opening)")

	SPECIAL_PROFIT_AND_LOSS = "SP", pgettext("account category", "Special (profit and loss)")


class Account(Model):

	# -----------
	# F I E L D S
	# -----------

	number = CharField(
		pgettext("account field", "Primary account number"),
		max_length=3,
		validators=[RegexValidator(r"^[\d]{3}$", message=pgettext("account validation", "Account number must consist of exactly three digits."))],
	)

	subnumber = CharField(
		pgettext("account field", "Subaccount number"),
		max_length=3,
		null=True, blank=True,
		default=None,
		validators=[RegexValidator(r"^[\d]{3}$", message=pgettext("account validation", "Account subnumber must consist of exactly three digits."))],
	)

	title = CharField(
		pgettext("account field", "Title"),
		max_length=100,
	)

	category = CharField(
		pgettext("account field", "Category"),
		max_length=2,
		choices=AccountCategory.choices,
	)

	enabled = BooleanField(
		pgettext("account field", "Enabled"),
		default=True,
	)

	# ---------------------------------------------------------
	# S T A N D A R D   A N D   B U S I N E S S   M E T H O D S
	# ---------------------------------------------------------

	def __str__(self) -> str:
		"""Return the string representation of this account"""
		return f"{self.compound_number} - {self.title}"

	@property
	@admin.display(ordering=Concat("number", Value("."), "subnumber"))
	def compound_number(self) -> str:
		if self.is_subaccount():
			return f"{self.number}.{self.subnumber}"
		else:
			return f"{self.number}"

	def is_subaccount(self) -> bool:
		"""Check whether this account is a subaccount"""
		return self.subnumber is not None

	# ---------------------
	# V A L I D A T I O N S
	# ---------------------

	def clean(self) -> None:
		"""Validate this account"""
		super().clean()

		# Primary account must exist for subaccounts
		if self.is_subaccount():
			query = Account.objects.filter(
				Q(number=self.number) &
				Q(subnumber__isnull=True)
			)

			# Exclude itself
			query = query.exclude(
				Q(pk=self.pk)
			)

			if not query.exists():
				raise ValidationError({
					"number": [ pgettext("account validation", "Primary account does not exist.") ]
				})

	def validate_unique(self, exclude: list[str] = None) -> None:
		"""Validate the uniqueness of this account"""
		super().validate_unique(exclude=exclude)

		# Subaccounts must be have a unique subnumber within the number of their primary account
		if self.is_subaccount():
			query = Account.objects.filter(
				Q(number=self.number) &
				Q(subnumber=self.subnumber)
			)

		# Primary accounts must have a unique number
		else:
			query = Account.objects.filter(
				Q(number=self.number) &
				Q(subnumber__isnull=True)
			)

		if self.pk is not None:
			# Exclude itself
			query = query.exclude(
				Q(pk=self.pk)
			)

		if query.exists():
			raise ValidationError({
				NON_FIELD_ERRORS: [ pgettext("account validation", "Account already exists.") ]
			})


@receiver(pre_delete, sender=Account)
def account_pre_delete_handler(sender: Account, instance: Account, *args, **kwargs) -> None: # pylint: disable=unused-argument
	"""Check whether accounts may be deleted"""

	# Accounts may not be deleted if used anytime in the past
	# (this is actually enforced through referential integrity constraint on Transaction model)

	# Primary accounts may not be deleted if subaccounts exists for them
	if not instance.is_subaccount():
		query = Account.objects.filter(
			Q(number=instance.number) &
			Q(subnumber__isnull=False)
		)

		if query.exists():
			raise ValidationError({
				NON_FIELD_ERRORS: [ pgettext("account validation", "Account contains subaccounts.") ]
			})

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class AccountingPeriodState(TextChoices):

	FUTURE = "F", pgettext("accounting period type", "Future")

	CURRENT = "C", pgettext("accounting period type", "Current")

	PAST = "P", pgettext("accounting period type", "Past")


class AccountingPeriod(Model):

	# -----------
	# F I E L D S
	# -----------

	title = CharField(
		pgettext("accounting period field", "Title"),
		max_length=50,
		unique=True,
	)

	opening_day = DateField(
		pgettext("accounting period field", "Opening date"),
	)

	closing_day = DateField(
		pgettext("accounting period field", "Closing date"),
	)

	state = CharField(
		pgettext("accounting period field", "State"),
		max_length=1,
		choices=AccountingPeriodState.choices,
		default=AccountingPeriodState.FUTURE,
	)

	# ---------------------------------------------------------
	# S T A N D A R D   A N D   B U S I N E S S   M E T H O D S
	# ---------------------------------------------------------

	def __str__(self) -> str:
		"""Return the string representation of this accounting period"""
		date_format = formats.get_format('SHORT_DATE_FORMAT', lang=get_language())

		return f"{self.title} ({dateformat.format(self.opening_day, date_format)} – {dateformat.format(self.closing_day, date_format)})"

	# ---------------------
	# V A L I D A T I O N S
	# ---------------------

	def clean_fields(self, exclude: list[str] = None) -> None:
		"""Validate the fields of this accounting period"""
		super().clean_fields(exclude=exclude)

		# Closing day must not precede opening day
		if self.closing_day < self.opening_day:
			raise ValidationError({
				"closing_day": [ pgettext("accounting period validation", "Closing day precedes opening day.") ]
			})

	def clean(self) -> None:
		"""Validate this accounting period"""
		super().clean()

		if self.pk:
			original_instance = AccountingPeriod.objects.get(
				Q(pk=self.pk)
			)

			# Past accounting periods may not be changed
			if original_instance.state == AccountingPeriodState.PAST:
				raise ValidationError({
					NON_FIELD_ERRORS: [ pgettext("accounting period validation", "Past accounting periods may not be changed.") ]
				})

			# Opening day may not be changed
			if self.opening_day != original_instance.opening_day:
				raise ValidationError({
					"opening_day": [ pgettext("accounting period validation", "Opening day may not be changed.") ]
				})

			# Closing day must not be changed to a value preceding the last booked transaction
			latest_booking_date = Transaction.objects.filter(
				Q(accounting_period=self)
			).latest("booking_date").booking_date

			if self.closing_day < latest_booking_date:
				raise ValidationError({
					"closing_day": [ pgettext("accounting period validation", "Closing day precedes booked transactions.") ]
				})

	def validate_unique(self, exclude: list[str] = None) -> None:
		"""Validate the uniqueness of this accounting period"""
		super().validate_unique(exclude=exclude)

		# Multiple current accounting periods are not allowed
		query = AccountingPeriod.objects.filter(
			Q(state=AccountingPeriodState.CURRENT)
		)

		if self.pk is not None:
			# Exclude itself
			query = query.exclude(
				Q(pk=self.pk)
			)

		if query.exists():
			raise ValidationError({
				NON_FIELD_ERRORS: [ pgettext("accounting period validation", "There is already a current accounting period.") ]
			})

		# Multiple future accounting periods are not allowed
		query = AccountingPeriod.objects.filter(
			Q(state=AccountingPeriodState.FUTURE)
		)

		if self.pk is not None:
			# Exclude itself
			query = query.exclude(
				Q(pk=self.pk)
			)

		if query.exists():
			raise ValidationError({
				NON_FIELD_ERRORS: [ pgettext("accounting period validation", "There is already a future accounting period.") ]
			})

		# Accounting periods must not overlap
		query = AccountingPeriod.objects.filter(
			(Q(opening_day__gte=self.opening_day) & (Q(opening_day__lte=self.closing_day))) |
			(Q(closing_day__gte=self.opening_day) & (Q(closing_day__lte=self.closing_day))) |
			(Q(opening_day__lt=self.opening_day) & (Q(closing_day__gt=self.closing_day)))
		)

		if self.pk is not None:
			# Exclude itself
			query = query.exclude(
				Q(pk=self.pk)
			)

		if query.exists():
			raise ValidationError({
				NON_FIELD_ERRORS: [ pgettext("accounting period validation", "Period overlaps with another one.") ]
			})


@receiver(pre_delete, sender=AccountingPeriod)
def accounting_period_pre_delete_handler(sender: AccountingPeriod, instance: AccountingPeriod, *args, **kwargs) -> None: # pylint: disable=unused-argument
	"""Check whether accounting periods may be deleted"""

	# Only current and future accounting periods may be deleted
	if instance.state not in { AccountingPeriodState.CURRENT, AccountingPeriodState.FUTURE }:
		raise ValidationError({
			NON_FIELD_ERRORS: [ pgettext("accounting period validation", "Past accounting periods may not be deleted.") ]
		})

	# Accounting periods may not be deleted if they contain any transactions
	# (this is actually enforced through referential integrity constraint on Transaction model)

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class Accountant(AbstractUser):
	pass

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class DocumentType(Model):

	# -----------
	# F I E L D S
	# -----------

	abbreviation = CharField(
		pgettext("document type field", "Abbreviation"),
		max_length=3,
		unique=True,
	)

	title = CharField(
		pgettext("document type field", "Title"),
		max_length=50,
		unique=True,
	)

	# ---------------------------------------------------------
	# S T A N D A R D   A N D   B U S I N E S S   M E T H O D S
	# ---------------------------------------------------------

	def __str__(self) -> str:
		"""Return the string representation of this document type"""
		return self.abbreviation

	# ---------------------
	# V A L I D A T I O N S
	# ---------------------

	# Document types may not be deleted if used anytime in the past
	# (this is actually enforced through referential integrity constraint on Transaction model)

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class Transaction(Model):

	# -----------
	# F I E L D S
	# -----------

	sequence_number = PositiveIntegerField(
		pgettext("transaction field", "Sequence number"),
		null=True,
		default=None,
		unique=True,
	)

	accounting_period = ForeignKey(
		AccountingPeriod,
		verbose_name = pgettext("transaction field", "Accounting period"),
		on_delete=PROTECT,
	)

	booking_date = DateField(
		pgettext("transaction field", "Booking date"),
		default=localdate,
	)

	document_type = ForeignKey(
		DocumentType,
		verbose_name = pgettext("transaction field", "Document type"),
		on_delete=PROTECT,
		related_name="+",
	)

	document_number = PositiveIntegerField(
		pgettext("transaction field", "Document number"),
	)

	description = TextField(
		pgettext("transaction field", "Description"),
		max_length=255
	)

	amount = DecimalField(
		pgettext("transaction field", "Amount"),
		max_digits=17,
		decimal_places=2,
	)

	debit_account = ForeignKey(
		Account,
		verbose_name = pgettext("transaction field", "Debit account"),
		on_delete=PROTECT,
		related_name="+",
	)

	credit_account = ForeignKey(
		Account,
		verbose_name = pgettext("transaction field", "Credit account"),
		on_delete=PROTECT,
		related_name="+",
	)

	business_partner = CharField(
		pgettext("transaction field", "Business partner"),
		max_length=100,
		null=True, blank=True,
		default=None,
	)

	tax_deductible_expense = BooleanField(
		pgettext("transaction field", "Tax-deductible expense"),
		default=True,
	)

	remark = TextField(
		pgettext("transaction field", "Remark"),
		max_length=5000,
		null=True, blank=True,
		default=None,
	)

	booked_by = ForeignKey(
		Accountant,
		verbose_name = pgettext("transaction field", "Booked by"),
		on_delete=PROTECT,
	)

	# ---------------------------------------------------------
	# S T A N D A R D   A N D   B U S I N E S S   M E T H O D S
	# ---------------------------------------------------------

	def __str__(self) -> str:
		"""Return the string representation of this transaction"""
		document_reference = f"{self.document_type.abbreviation}.{self.document_number}"
		debit_account_compound_number = f"{self.debit_account.compound_number}"
		credit_account_compound_number = f"{self.credit_account.compound_number}"

		return f"{self.booking_date}, {document_reference}: {self.amount} ({debit_account_compound_number}/{credit_account_compound_number})"

	# ---------------------
	# V A L I D A T I O N S
	# ---------------------

	def clean(self) -> None:
		"""Validate this accounting period"""
		super().clean()

		# Accounting period may not be past
		if hasattr(self, "accounting_period"):
			if self.accounting_period.state == AccountingPeriodState.PAST:
				raise ValidationError({
					"accounting_period": [ pgettext("transaction validation", "Past accounting periods may not be modified.") ]
				})

		# Booking date may not fall outside the accounting period
		if hasattr(self, "accounting_period"):
			if not self.accounting_period.opening_day <= self.booking_date <= self.accounting_period.closing_day:
				raise ValidationError({
					"booking_date": [ pgettext("transaction validation", "Booking date is outside the accounting period.") ]
				})

		# Transaction may not use primary accounts if subaccounts exist
		if hasattr(self, "debit_account"):
			if not self.debit_account.is_subaccount():
				query = Account.objects.filter(
					Q(number=self.debit_account.number) &
					Q(subnumber__isnull=False)
				)

				if query.exists():
					raise ValidationError({
						"debit_account": [ pgettext("transaction validation", "Subaccount must be used.") ]
					})

		if hasattr(self, "credit_account"):
			if not self.credit_account.is_subaccount():
				query = Account.objects.filter(
					Q(number=self.credit_account.number) &
					Q(subnumber__isnull=False)
				)

				if query.exists():
					raise ValidationError({
						"credit_account": [ pgettext("transaction validation", "Subaccount must be used.") ]
					})

		# Correct tax-deductible expense flag for non-expense transactions
		if hasattr(self, "debit_account"):
			if not self.debit_account.category != AccountCategory.EXPENSE:
				self.tax_deductible_expense = False


@receiver(pre_delete, sender=Transaction)
def transaction_pre_delete_handler(sender: Transaction, instance: Transaction, *args, **kwargs) -> None: # pylint: disable=unused-argument
	"""Disable deleting of booked transactions"""

	raise ValidationError({
		NON_FIELD_ERRORS: [ pgettext("transaction validation", "Booked transactions may not be deleted.") ]
	})

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class GeneralLedgerEntry(Model):

	class Meta:

		db_table = "core_general_ledger" # Database-level view into Transaction records

		managed = False

	# -----------
	# F I E L D S
	# -----------

	sequence_number = PositiveIntegerField()

	accounting_period = ForeignKey(AccountingPeriod, on_delete=DO_NOTHING)

	booking_date = DateField()

	document_type = ForeignKey(DocumentType, on_delete=DO_NOTHING)

	document_number = PositiveIntegerField()

	description = TextField()

	account = ForeignKey(Account, on_delete=DO_NOTHING)

	amount_debit = DecimalField(max_digits=17, decimal_places=2)

	amount_credit = DecimalField(max_digits=17, decimal_places=2)

	business_partner = CharField(max_length=100)

	tax_deductible_expense = BooleanField()

	remark = TextField(	)

	booked_by = ForeignKey(Accountant, on_delete=DO_NOTHING)

	# ---------------------------------------------------------
	# S T A N D A R D   A N D   B U S I N E S S   M E T H O D S
	# ---------------------------------------------------------

	def delete(self, *args, **kwargs) -> None:
		raise NotImplementedError("GeneralLedgerEntry is read-only")

	def save(self, *args, **kwargs) -> None:
		raise NotImplementedError("GeneralLedgerEntry is read-only")
