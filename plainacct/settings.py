from os import environ as env
from pathlib import Path

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

ALLOWED_HOSTS = env.get("DJANGO_ALLOWED_HOSTS", "*").split(" ")

DEBUG = env.get("DJANGO_DEBUG", "True") == "True"

LANGUAGE_CODE = env.get("DJANGO_LANGUAGE_CODE", "cs")

SECRET_KEY = env.get("DJANGO_SECRET_KEY", "qq%@77+w9c-hkqv0-734z(m3xy!h526+g9oxdy&*h%_4h6f*_)")

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------


# ----------------------
# Application definition
# ----------------------

BASE_DIR = Path(__file__).resolve().parent.parent

INSTALLED_APPS = [
	"plainacct.core",
	"django.contrib.admin",
	"django.contrib.auth",
	"django.contrib.contenttypes",
	"django.contrib.sessions",
	"django.contrib.messages",
	"django.contrib.staticfiles",
]

MIDDLEWARE = [
	"django.middleware.security.SecurityMiddleware",
	"django.contrib.sessions.middleware.SessionMiddleware",
	"django.middleware.common.CommonMiddleware",
	"django.middleware.csrf.CsrfViewMiddleware",
	"django.contrib.auth.middleware.AuthenticationMiddleware",
	"django.contrib.messages.middleware.MessageMiddleware",
	"django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "plainacct.urls"

WSGI_APPLICATION = "plainacct.wsgi.application"


# -----------------------
# Authentication settings
# -----------------------

AUTH_PASSWORD_VALIDATORS = [
	{ "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator", },
	{ "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator", },
	{ "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator", },
	{ "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator", },
]

AUTH_USER_MODEL = "core.Accountant"

LOGIN_REDIRECT_URL = "core_dashboard"

LOGOUT_REDIRECT_URL = "login"

# -----------------
# Database settings
# -----------------

DATABASES = {
	"default": {
		"ENGINE": env.get("DJANGO_SQL_ENGINE", "django.db.backends.sqlite3"),
		"HOST": env.get("DJANGO_SQL_HOST", ""),
		"PORT": env.get("DJANGO_SQL_PORT", ""),
		"NAME": env.get("DJANGO_SQL_DB", BASE_DIR / "db.sqlite3"),
		"USER": env.get("DJANGO_SQL_USER", ""),
		"PASSWORD": env.get("DJANGO_SQL_PASSWORD", ""),
	}
}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"


# -----------------------------
# Internationalization settings
# -----------------------------

LOCALE_PATHS = [
	BASE_DIR / "plainacct" / "locale"
]

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# -----------------
# Resource settings
# -----------------

STATIC_URL = "/static/"

STATIC_ROOT = BASE_DIR / "static"

TEMPLATES = [
	{
		"BACKEND": "django.template.backends.django.DjangoTemplates",
		"DIRS": [],
		"APP_DIRS": True,
		"OPTIONS": {
			"context_processors": [
				"django.template.context_processors.debug",
				"django.template.context_processors.request",
				"django.contrib.auth.context_processors.auth",
				"django.contrib.messages.context_processors.messages",
			],
		},
	},
]


# ----------------
# Session settings
# ----------------

SESSION_COOKIE_HTTPONLY = True

SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"

SESSION_SERIALIZER = "django.contrib.sessions.serializers.PickleSerializer"
