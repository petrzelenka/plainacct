from django.core.paginator import InvalidPage
from django.core.paginator import Page
from django.core.paginator import Paginator
from django.db.models.query import QuerySet
from django.views.generic.list import ListView

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class LastPageByDefaultListView(ListView):

	def paginate_queryset(self, queryset: QuerySet, page_size: int) -> tuple[Paginator, Page, QuerySet, bool]:
		paginator = self.get_paginator(
			queryset,
			page_size,
			orphans=self.get_paginate_orphans(),
			allow_empty_first_page=self.get_allow_empty()
		)
		page_number = self.kwargs.get(self.page_kwarg) or self.request.GET.get(self.page_kwarg)

		try:
			page = paginator.page(page_number)
		except InvalidPage:
			page = paginator.page(paginator.num_pages)

		return paginator, page, page.object_list, page.has_other_pages()
