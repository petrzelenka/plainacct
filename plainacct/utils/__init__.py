from typing import Any

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

def replace_none_by_default(original: dict[str, Any], default: dict[str, Any]) -> dict[str, Any]:
	"""Replace any mappings to None with the corresponding default values"""
	none_removed = { key: value for key, value in original.items() if value }

	return default | none_removed
