from django.forms import Form
from django.forms import Select

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

class StyledErrorForm(Form):

	def is_valid(self):
		result = super().is_valid()

		if "__all__" in self.errors:
			invalid_fields = self.fields
		else:
			invalid_fields = self.errors

		for field_name in invalid_fields:
			widget = self.fields[field_name].widget
			if not isinstance(widget, Select):
				attrs = widget.attrs

				css_classes = attrs.get("class", "")
				css_classes += f" {self.error_css_class}"
				css_classes = css_classes.strip()

				attrs.update({ "class": css_classes })

		return result
