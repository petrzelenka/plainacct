from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import PasswordChangeDoneView
from django.contrib.auth.views import PasswordChangeView
from django.urls import include
from django.urls import path
from django.views.generic.base import RedirectView

from plainacct.core.forms import LoginForm
from plainacct.core.forms import PasswordChangeForm

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------

urlpatterns = [
	path("", RedirectView.as_view(url="core/", permanent=True), name="dashboard"),
	path("accounts/login/", LoginView.as_view(authentication_form=LoginForm), name="login"),
	path("accounts/password_change/", PasswordChangeView.as_view(form_class=PasswordChangeForm), name="password_change"),
	path("accounts/password_change_done/", PasswordChangeDoneView.as_view(), name="password_change_done"),
	path("accounts/", include("django.contrib.auth.urls")),
	path("admin/", admin.site.urls),
	path("core/", include("plainacct.core.urls")),
]
